package ncdc.recruitment.webApplication.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ncdc.recruitment.webApplication.entities.Book;

@Repository
public interface BookRepository extends CrudRepository<Book, Integer> {
	Optional<Book> findBookByIsbn(String isbn);
}
