package ncdc.recruitment.webApplication.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Not found books in data base")
public class NotFoundBooksException extends Exception{
	private static final long serialVersionUID = -2779203816335863267L;
}
