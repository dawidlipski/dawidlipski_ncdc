package ncdc.recruitment.webApplication.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import ncdc.recruitment.webApplication.DTO.BookDTO;
import ncdc.recruitment.webApplication.controller.BookController;
import ncdc.recruitment.webApplication.entities.Book;
import ncdc.recruitment.webApplication.exceptions.BookAlreadyExistsException;
import ncdc.recruitment.webApplication.exceptions.NotFoundBooksException;
import ncdc.recruitment.webApplication.repositories.BookRepository;

@Service
public class BookService {
	
	private static final Logger LOG = Logger.getLogger(BookController.class);
	
	@Autowired
	private BookRepository bookRepository;
	
	public String createErrorString(BindingResult bindingResult) { 
		List<String> error= new ArrayList<>();
		for(ObjectError errors:bindingResult.getAllErrors()) {
			error.add(errors.getDefaultMessage());
		}
		
		return String.join(", ", error);
    }
	
	public String addNewBook(BookDTO book) throws BookAlreadyExistsException {
		Book newBook= new Book();
		Optional<Book> isbn = bookRepository.findBookByIsbn(book.getIsbn());
		if(!isbn.isPresent()) {
			LOG.info(String.format("Saving new Book to data base: %s", newBook));
			newBook.setIsbn(book.getIsbn());
			newBook.setAuthor(book.getAuthor());
			newBook.setTitle(book.getTitle());
			bookRepository.save(newBook);
		}else {
			LOG.info(String.format("Found same ISBN in data base  %s", isbn));
			throw new BookAlreadyExistsException();
		}
		
		return "Added new book to data base";
	}
	
	public List<Book> getAllBooks() throws NotFoundBooksException {
		List<Book> books= (List<Book>) bookRepository.findAll();
		if(books.isEmpty()) {
			LOG.info("Not found books in data base");
			throw new NotFoundBooksException();
		}
		LOG.info(String.format("Return list of all books", books));
		
		return books;
	}
}
