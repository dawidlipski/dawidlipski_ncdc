package ncdc.recruitment.webApplication.service;

import java.util.Optional;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import ncdc.recruitment.webApplication.controller.BookApplicationController;
import ncdc.recruitment.webApplication.entities.Book;
import ncdc.recruitment.webApplication.repositories.BookRepository;

@Service
public class BookApplicationService {
	
	private static final Logger LOG = Logger.getLogger(BookApplicationController.class);
	
	@Autowired
	public BookRepository bookRepository;
	
	public boolean checkBookObjectIsNull(Book book) {
		if(book.getAuthor() == null){
			return true;
		}
		if(book.getIsbn() == null){
			return true;
		}
		if(book.getTitle() == null){
			return true;
		}
		
		return false;
	}
	
	public String addBook(Model model, Book book, BindingResult bindingResult, Errors error) {
		model.addAttribute(book);
		Optional<Book> isbn= bookRepository.findBookByIsbn(book.getIsbn());
		if(checkBookObjectIsNull(book)) {
			LOG.info(String.format("All fields are nulls: %s", book));
			return "addBook";
		}else if(bindingResult.hasErrors() || isbn.isPresent()) {
			if(isbn.isPresent())
			{
				LOG.info(String.format("Found same ISBN in data base: %s", isbn));
				error.rejectValue("isbn", "isbn.exist", "Book already Exists");
			}
			
			return "addBook";		
		}else {
			LOG.info(String.format("Saving new Book to data base: %s", book));
			bookRepository.save(book);
			
			return "redirect:/";
		}	
	}
	
	public String showBooks(Model model, Book book) {
		model.addAttribute("books", bookRepository.findAll());
		LOG.info(String.format("Show all books from data base"));
		
	    return "showBooks";
	}
}
