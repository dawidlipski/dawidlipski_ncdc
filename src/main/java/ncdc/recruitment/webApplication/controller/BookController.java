package ncdc.recruitment.webApplication.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ncdc.recruitment.webApplication.DTO.BookDTO;
import ncdc.recruitment.webApplication.entities.Book;
import ncdc.recruitment.webApplication.exceptions.BookAlreadyExistsException;
import ncdc.recruitment.webApplication.exceptions.NotFoundBooksException;
import ncdc.recruitment.webApplication.service.BookService;

@RestController
@RequestMapping("/book")
public class BookController {
	
	@Autowired
	private BookService bookService;
	
	@PostMapping("/addBook")
	public ResponseEntity<String> addBook(@Valid @RequestBody BookDTO book, BindingResult bindingResult) throws BookAlreadyExistsException{
		if(bindingResult.hasErrors()) {
			return new ResponseEntity<>(bookService.createErrorString(bindingResult), HttpStatus.BAD_REQUEST);
		}
		try {
			return new ResponseEntity<>(bookService.addNewBook(book), HttpStatus.CREATED);
		} catch (BookAlreadyExistsException e) {
			throw e;
		}
	}
	
	@GetMapping("/getAllBooks")
	public ResponseEntity<List<Book>> getAllBooks() throws NotFoundBooksException{
		try {
			return new ResponseEntity<>(bookService.getAllBooks(), HttpStatus.OK);
		} catch (NotFoundBooksException e) {
			throw e;
		}
	}
}
