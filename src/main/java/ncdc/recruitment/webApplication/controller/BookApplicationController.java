package ncdc.recruitment.webApplication.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import ncdc.recruitment.webApplication.entities.Book;
import ncdc.recruitment.webApplication.service.BookApplicationService;

@Controller
public class BookApplicationController {
	
	@Autowired
	private BookApplicationService bookApplicationService;
	
	@GetMapping("/addBook")
	public String addBook(Model model, Book book) {
		return "addBook";
	}
	
	@PostMapping("/addBook")
	public String formSumbit(@Valid Book book, BindingResult bindingResult, Model model, Errors error) {
		return bookApplicationService.addBook(model, book, bindingResult, error);
	}
	
	@GetMapping("/")
	public String showBooks(Model model, Book book) {
		return bookApplicationService.showBooks(model, book);
	}
}
