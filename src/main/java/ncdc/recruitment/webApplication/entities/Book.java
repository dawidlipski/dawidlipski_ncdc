package ncdc.recruitment.webApplication.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
public class Book {
	@Id
	@GeneratedValue
	private Integer id;
	@Size(min=1, max=32, message="Author name must be between 1 and 32 characters")
	@Pattern(regexp = "([A]+[a-zA-Z]*)|([A-Z]+[a-zA-Z]* [A]+[a-zA-Z]*)|([A]+[a-zA-Z]* [A-Z][a-zA-Z]*)$", message="Forename or surename should start from letter 'A' and start with capital letter")
	private String author;
	@Size(min=1, message="Title can not be null")
	private String title;
	@Pattern(regexp = "([0-9]{10})|([0-9]{13})$", message="ISBN must consist of 10 or 13 numbers")
	private String isbn;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getIsbn() {
		return isbn;
	}
	
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
	@Override
	public String toString() 
	{
	    return "Author: "+getAuthor()+" Title: "+getTitle()+" ISBN: "+getIsbn(); 
	}
}
